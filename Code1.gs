/*
     Script made by Andrei Ciobanu
     Update Dashboard - Search Query Performance Report Script
*/
function main() {
  
   var array_info=[];  
   var array_info = get_default_values();  
   populate_Dashboard(array_info);
   design_dashboard();
  	
}

function populate_Dashboard(array_info_2)
{
  	Logger.log("inceput");
  	var SPREADSHEET_ID_IMPORT_2 ='1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8'; //Trial8
  	var SPREADSHEET_URL_IMPORT_2 ='https://drive.google.com/open?id=1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8';
	var ss_IMPORT_2 = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT_2);
  	var SHEET_NAME_IMPORT_2 = 'Dashboard';
	var sheet_IMPORT_2 = ss_IMPORT_2.getSheetByName(SHEET_NAME_IMPORT_2);  
  
  	sheet_IMPORT_2.clearContents();
   
    SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT_2);
    ss_IMPORT_2.setActiveSheet(sheet_IMPORT_2);
	var sheet_2 = SpreadsheetApp.getActiveSheet();  
  	var accountIterator_2 = MccApp.accounts().get();
	var accountCounter_2 = 0;
    var row_2,column_2,range_2;
  
  	row_2=1;column_2=1; 
  range_2=sheet_2.getRange(row_2,column_2,1,1);range_2.setValue('Name of account'); range_2.setHorizontalAlignment("center"); column_2++;
  range_2=sheet_2.getRange(row_2,column_2,1,1);range_2.setValue('Account ID');  range_2.setHorizontalAlignment("center"); column_2++;
  range_2=sheet_2.getRange(row_2,column_2,1,1);range_2.setValue('Enable script?');  range_2.setHorizontalAlignment("center"); column_2++;
  range_2=sheet_2.getRange(row_2,column_2,1,1);range_2.setValue('Lower conversions limit (Editable)');  range_2.setHorizontalAlignment("center"); column_2++;
  range_2=sheet_2.getRange(row_2,column_2,1,1);range_2.setValue('Time (in days)(Editable)');  range_2.setHorizontalAlignment("center");
  
    row_2=2;
  	while (accountIterator_2.hasNext())
		  {  
            var account_2 = accountIterator_2.next();
			MccApp.select(account_2);
            accountCounter_2++;
         
          column_2=1;
          range_2 = sheet_2.getRange(row_2,column_2,1,1); range_2.setValue(account_2.getName().toString());  range_2.setHorizontalAlignment("center");column_2++;
          range_2 = sheet_2.getRange(row_2,column_2,1,1); range_2.setValue(account_2.getCustomerId().toString());  range_2.setHorizontalAlignment("center");column_2++;
          range_2 = sheet_2.getRange(row_2,column_2,1,1); array_info_2[4].copyTo(range_2); range_2.setValue(array_info_2[3]);  range_2.setHorizontalAlignment("center");column_2++;
          range_2 = sheet_2.getRange(row_2,column_2,1,1); range_2.setValue(array_info_2[2].toString());  range_2.setHorizontalAlignment("center");column_2++;
          range_2 = sheet_2.getRange(row_2,column_2,1,1); range_2.setValue(array_info_2[1].toString());  range_2.setHorizontalAlignment("center");
            
          row_2++; 
          }
Logger.log('gata');
Logger.log(accountCounter_2);
}


function get_default_values()
{
	var SPREADSHEET_ID_IMPORT_3 ='1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8'; //Trial8
  	var SPREADSHEET_URL_IMPORT_3 ='https://drive.google.com/open?id=1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8';
	var ss_IMPORT_3 = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT_3);
  	var SHEET_NAME_IMPORT_3 = 'Cover Page';
	var sheet_IMPORT_3 = ss_IMPORT_3.getSheetByName(SHEET_NAME_IMPORT_3); 
  	var information=[];
   
    SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT_3);
    ss_IMPORT_3.setActiveSheet(sheet_IMPORT_3);
	var sheet_3 = SpreadsheetApp.getActiveSheet(); 
  
  	var default_email = sheet_3.getRange(6,5,1,1).getValue().toString();
    var default_time =  sheet_3.getRange(7,5,1,1).getValue();
  	var default_limit = sheet_3.getRange(8,5,1,1).getValue();
  	var default_enable_value = sheet_3.getRange(9,5,1,1).getValue();
    var default_enable_tickbox = sheet_3.getRange(9,5,1,1);
   
  

	information.push(default_email,default_time,default_limit,default_enable_value,default_enable_tickbox);
  Logger.log("sfarsit");
    return information;
}



function design_dashboard()
{
  Logger.log('buna');
    // Here is the DESIGN part
        var SPREADSHEET_ID_IMPORT_4 ='1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8'; //Trial8
  		var SPREADSHEET_URL_IMPORT_4 ='https://drive.google.com/open?id=1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8';
		var ss_IMPORT_4 = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT_4);
  		var SHEET_NAME_IMPORT_4 = 'Dashboard';
		var sheet_IMPORT_4 = ss_IMPORT_4.getSheetByName(SHEET_NAME_IMPORT_4);  
   
    	SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT_4);
   	 	ss_IMPORT_4.setActiveSheet(sheet_IMPORT_4);
		var sheet_4 = SpreadsheetApp.getActiveSheet(); 
            
           var range1 = sheet_4.getRange('A1:E1');
		   var rule1 = SpreadsheetApp.newConditionalFormatRule()  
  			.whenCellNotEmpty()
  			.setBackground("#afb8bc")
  			.setRanges([range1])
  			.build();
          
          
           var range2 = sheet_4.getRange('A2:B300');
           var range3 = sheet_4.getRange('D2:E300'); 
           var rule2 = SpreadsheetApp.newConditionalFormatRule()  
  			.whenCellNotEmpty()
  			.setBackground("#eff1f2")
  			.setRanges([range2,range3])
  			.build();
          
      
        
  		   var rules = sheet_4.getConditionalFormatRules();
  	       rules.push(rule1);rules.push(rule2);
           sheet_4.setConditionalFormatRules(rules);
    
           sheet_4.sort(1, true);
    	   sheet_4.setTabColor("483ae8");
           sheet_4.setFrozenRows(1);
            
}

