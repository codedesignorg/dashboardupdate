//============
// Update Dashboard for the Exact Match Script
// Made by Andrei     
//============

function main() {
  
   var array_info=[];  
   var array_info = get_default_values();  
   populate_Dashboard(array_info);
   design_dashboard();
  
}

function populate_Dashboard(array_info)
{
	Logger.log("inceput Populate Dashboard");
	var SPREADSHEET_ID_IMPORT ='1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
    var SPREADSHEET_URL_IMPORT ='https://drive.google.com/open?id=1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
	var ss_IMPORT = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT);
  	var SHEET_NAME_IMPORT = 'Dashboard';
	var sheet_IMPORT = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT);
  
  sheet_IMPORT.clearContents();
  
  SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT);
  ss_IMPORT.setActiveSheet(sheet_IMPORT);
  var sheet = SpreadsheetApp.getActiveSheet();
  var accountIterator = MccApp.accounts().get();
  var accountCounter = 0;
  var row,column,range;
  
  
  row=1;column=1;
  range = sheet.getRange(row,column,1,1);range.setValue('Name of account'); range.setHorizontalAlignment("center");column++;
  range = sheet.getRange(row,column,1,1);range.setValue('Account ID');  range.setHorizontalAlignment("center"); column++;
  range = sheet.getRange(row,column,1,1);range.setValue('Enable script');  range.setHorizontalAlignment("center"); column++;
  range = sheet.getRange(row,column,1,1);range.setValue('Time'); range.setHorizontalAlignment("center");
  
  row=2;
  while (accountIterator.hasNext())
		{  
          
          var account = accountIterator.next();
		  MccApp.select(account);
          accountCounter++;
         
          column=1;
          range = sheet.getRange(row,column,1,1); range.setValue(account.getName().toString());  range.setHorizontalAlignment("center");column++;
          range = sheet.getRange(row,column,1,1); range.setValue(account.getCustomerId().toString());  range.setHorizontalAlignment("center");column++;
          range = sheet.getRange(row,column,1,1); array_info[2].copyTo(range); range.setValue(array_info[1]); range.setHorizontalAlignment("center"); column++;
          range = sheet.getRange(row,column,1,1); range.setValue(array_info[3]); range.setHorizontalAlignment("center");
         
          row++; 
          }
  
  Logger.log(accountCounter);
  Logger.log("gata cu populate Dashboard");
  
}

function get_default_values() 
{
	Logger.log('inceput get default values');	
  var SPREADSHEET_ID_IMPORT ='1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
  	var SPREADSHEET_URL_IMPORT ='https://drive.google.com/open?id=1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
	var ss_IMPORT = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT);
  	var SHEET_NAME_IMPORT = 'Cover Page';
	var sheet_IMPORT = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT); 
  	var information=[];
   
    SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT);
    ss_IMPORT.setActiveSheet(sheet_IMPORT);
	var sheet = SpreadsheetApp.getActiveSheet(); 
  
	var default_email = sheet.getRange(5,5,1,1).getValue().toString();  
    var default_enable_value = sheet.getRange(6,5,1,1).getValue();
    var default_enable_tickbox = sheet.getRange(6,5,1,1);
  	var default_time = sheet.getRange(7,5,1,1).getValue().toString();
  
  	information.push(default_email,default_enable_value,default_enable_tickbox,default_time);
    Logger.log("sfarsit get_default_values");
    return information;
}

function design_dashboard()
{
  Logger.log('inceput design part');
 
    var SPREADSHEET_ID_IMPORT ='1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
  	var SPREADSHEET_URL_IMPORT ='https://drive.google.com/open?id=1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
	var ss_IMPORT = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT);
  	var SHEET_NAME_IMPORT = 'Dashboard';
	var sheet_IMPORT = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT);  
   
    SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT);
   	ss_IMPORT.setActiveSheet(sheet_IMPORT);
	var sheet = SpreadsheetApp.getActiveSheet(); 
            
    var range1 = sheet.getRange('A1:D1');
	var rule1 = SpreadsheetApp.newConditionalFormatRule()  
  			.whenCellNotEmpty()
  			.setBackground("#afb8bc")
  			.setRanges([range1])
  			.build();
          
          
    var range2 = sheet.getRange('A2:B300');
    var range3 = sheet.getRange('D2:D300');
    var rule2 = SpreadsheetApp.newConditionalFormatRule()  
  			.whenCellNotEmpty()
  			.setBackground("#eff1f2")
  			.setRanges([range2,range3])
  			.build();
          
      
        
  	var rules = sheet.getConditionalFormatRules();
  	rules.push(rule1);rules.push(rule2);
    sheet.setConditionalFormatRules(rules);
    
    sheet.sort(1, true);
   	sheet.setTabColor("483ae8");
    sheet.setFrozenRows(1);
            Logger.log('sfarsit design part');
}
